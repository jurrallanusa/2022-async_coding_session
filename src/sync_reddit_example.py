import signal  
import sys  
import json
import random
import requests


def get_json(url):
    # When you don't set up an user agent, the requests library will use the default user agent.
    headers = { 'User-Agent': 'Async workshop demo'}
    return requests.get(url, headers=headers).json()

def get_reddit_top(subreddit):  
    # emphasize async behavior by adding more time
    print(f"Awaiting for {subreddit} request")

    data1 = get_json('https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=day&limit=5')
    
    print('-----------------------------------------------------')

    j = data1
    for i in j['data']['children']:
        score = i['data']['score']
        title = i['data']['title']
        link = i['data']['url']
        print(str(score) + ': ' + title)


    print('DONE:', subreddit + '\n')


# With aiohttp, you can use the ClientSession object make requests to the Reddit API.
# The following snippet shows how to make a GET request to the top posts of the /r/python subreddit.
def main():
    subreddits = ['python', 'programming', 'compsci']
    # Make requests for each subreddit
    for subreddit in subreddits:
        # make sync request
        get_reddit_top(subreddit)
    
# run main
if __name__ == '__main__':
    # run main
    main()