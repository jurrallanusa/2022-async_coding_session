import signal  
import sys  
import asyncio  
import aiohttp  
import json
import random

async def get_json(client, url, **kwargs):
    async with client.get(url) as response:
        assert response.status == 200
        return await response.read()

async def get_reddit_top(subreddit, client):  
    # emphasize async behavior by adding more time
    print(f"Awaiting for {subreddit} request")
    await asyncio.sleep(random.randint(1, 10))


    data1 = await get_json(client, 'https://www.reddit.com/r/' + subreddit + '/top.json?sort=top&t=day&limit=5')

    print('-----------------------------------------------------')

    j = json.loads(data1.decode('utf-8'))
    for i in j['data']['children']:
        score = i['data']['score']
        title = i['data']['title']
        link = i['data']['url']
        # print(str(score) + ': ' + title + ' (' + link + ')')
        print(str(score) + ': ' + title)


    print('DONE:', subreddit + '\n')


# With aiohttp, you can use the ClientSession object make requests to the Reddit API.
# The following snippet shows how to make a GET request to the top posts of the /r/python subreddit.
async def main():
    subreddits = ['python', 'programming', 'compsci']
    # async with can only be called inside an async function
    async with aiohttp.ClientSession() as session:
        tasks = set()
        print(type(tasks))

        for subreddit in subreddits:
            tasks.add(get_reddit_top(subreddit, session))

        # Gather tasks
        await asyncio.gather(*tasks)

# run main
if __name__ == '__main__':
    # run main
    asyncio.run(main())