# Notes on async programming
A practical definition of Async is that it’s a style of concurrent programming in which tasks release the CPU during waiting periods, so that other tasks can use it. In Python, there are several ways to achieve concurrency, based on our requirement, code flow, data manipulation, architecture design and use cases we can select any of these methods.

## Reddit example synchronous and asynchronous
Demonstrate a bit the code example, then elaborate on concepts and considerations.

## Concepts 
- Go to the image with [programming models explained](https://medium.com/velotio-perspectives/an-introduction-to-asynchronous-programming-in-python-af0189a88bbb)

**An event loop** manages and distributes the execution of different tasks. It registers them and handles distributing the flow of control between them.

**Coroutines (covered above)** are special functions that work similarly to Python generators, on await they release the flow of control back to the event loop. A coroutine needs to be scheduled to run on the event loop, once scheduled coroutines are wrapped in Tasks which is a type of Future.

**Futures** represent the result of a task that may or may not have been executed. This result may be an exception. (Futures are like "Promises" objects in javascript)

## Python knowledge to understand better asyncio
- Iterators, Generators and Coroutines

## More about async programming
### Event Loops, Tasks, and Coroutines
In the asyncio world we no longer only have one stack per thread. Instead each thread has an object called an Event Loop. How to set up, work with, and shut down an event loop is covered in Part 2. For now just assume that one exists. The event loop contains within it a list of objects called Tasks. Each Task maintains a single stack, and its own execution pointer as well.

[Extend more on this topic here](https://bbc.github.io/cloudfit-public-docs/asyncio/asyncio-part-1.html )

## Project example jupyterfair project
Critical example when uploading parts using the figshare upload service.

## References
[1] “Mocking Asynchronous Functions In Python,” Feb. 18, 2020. https://dino.codes/posts/mocking-asynchronous-functions-python/ (accessed Jul. 03, 2022).
[2] “Python Asyncio Part 3 – Asynchronous Context Managers and Asynchronous Iterators,” cloudfit-public-docs. https://bbc.github.io/cloudfit-public-docs/asyncio/asyncio-part-3.html (accessed Jul. 01, 2022).
[3] “Python Asyncio Part 1 – Basic Concepts and Patterns,” cloudfit-public-docs. https://bbc.github.io/cloudfit-public-docs/asyncio/asyncio-part-1.html (accessed Jul. 01, 2022).
[4] “Python Asyncio Part 2 – Awaitables, Tasks, and Futures,” cloudfit-public-docs. https://bbc.github.io/cloudfit-public-docs/asyncio/asyncio-part-2.html (accessed Jul. 01, 2022).
[5] “Cooperative Multitasking with Coroutines — PyMOTW 3.” https://pymotw.com/3/asyncio/coroutines.html (accessed Jun. 24, 2022).
[6] “Asynchronous HTTP Requests in Python with aiohttp and asyncio,” Twilio Blog. https://www.twilio.com/blog/asynchronous-http-requests-in-python-with-aiohttp (accessed Jun. 24, 2022).
[7] CBT Nuggets, How to Create an Async API Call with asyncio | Python, (Feb. 22, 2021). Accessed: Jun. 24, 2022. [Online Video]. Available: https://www.youtube.com/watch?v=t0JXiljpNRo
[8] R. Python, “asyncio.sleep() and Writing Your First Coroutine – Real Python.” https://realpython.com/lessons/asynciosleep-and-writing-your-first-coroutine/ (accessed Jun. 24, 2022).
[9] V. Technologies, “An Introduction to Asynchronous Programming in Python,” Velotio Perspectives, Jun. 24, 2020. https://medium.com/velotio-perspectives/an-introduction-to-asynchronous-programming-in-python-af0189a88bbb (accessed Jun. 24, 2022).


