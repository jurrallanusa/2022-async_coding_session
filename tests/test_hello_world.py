# Expect that hello world function prints hello world
from src.hello_world import *

def test_hello_world():
    assert say_hello() == "Hello World!"